package com.farmaciacharro.elrwarning;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.farmaciacharro.elrwarning.model.almacenaje.Robot;
import com.farmaciacharro.elrwarning.model.error.PaletteError;
import com.farmaciacharro.elrwarning.model.error.STRError;
import com.farmaciacharro.elrwarning.model.error.StatusError;
import com.farmaciacharro.elrwarning.model.error.USGError;

import net.icolino.util.log.Log;

public class ELRWarning {

	private final static Logger LOGGER = Logger
			.getLogger(Log.LOGNAME);
	
	private static final String FILE = "Resultados.txt";
	
	public static void main(String args[]) {
		try {
			Log.setup(ELRWarning.class.getSimpleName());
			File root = new File("files");

			if ((root != null) && (root.isDirectory())) {
				Calendar fechaInicial = Calendar.getInstance();
				Robot.parseFiles("");
				dirDirectory(root);
				Calendar fechaFinal = Calendar.getInstance();
				LOGGER.info("Parsing iniciado a las " + fechaInicial.getTime().toString());
				LOGGER.info("Parsing finalizado a las " + fechaFinal.getTime().toString());
				LOGGER.info("Duracion (ms): " + (fechaFinal.getTimeInMillis() - fechaInicial.getTimeInMillis()));
				
			} else {
				LOGGER.severe("Root folder is missing");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	private static boolean dirDirectory(File directory)
			throws FileNotFoundException {
		File[] files = directory.listFiles();
		Arrays.sort(files);
		LOGGER.info(directory.getName());
		if (files != null) {
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					dirDirectory(files[i]);
				} else {
					parserFile(files[i]);
				}
			}
		}
		return false;
	}

	private static boolean parserFile(File file) throws FileNotFoundException {
		LOGGER.info(file.getName());
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String line;
		BufferedWriter bw = null;
		try {
			File results = new File(FILE);
			bw = new BufferedWriter(new FileWriter(results, true));
			bw.write("\n" + file + "\n\n");
			Pattern pattern = Pattern
					.compile("(\\d+):(\\d+):(\\d+):(\\d+)\\s+:\\s+(\\d+);\\s+(RHO|PLC);\\s+MFS;\\s+(\\d+);\\s+(\\w+);\\s+(\\w+);\\s+(\\d+);\\s+(\\d+);\\s+(.+)");
			while ((line = br.readLine()) != null) {
				Matcher matcher = pattern.matcher(line);
				if (matcher.find()) {
					String hour = matcher.group(1);
					String minute = matcher.group(2);
					String second = matcher.group(3);
					String milliseconds = matcher.group(4);
					//String counter = matcher.group(5);
					//String version = matcher.group(6);
					String who = matcher.group(7);
					String unnamed = matcher.group(8);
					String origin = matcher.group(9);
					String error = matcher.group(10);
					//String pending = matcher.group(11);
					String message = matcher.group(12);
					com.farmaciacharro.elrwarning.model.error.Error specificError = null;
					if (!who.equals("00")) {
						if (origin.equals("STR")) {
							specificError = new STRError(hour, minute, second, milliseconds, who, error, message);
						} else if ((unnamed.equals("USG")) && (origin.equals("STF"))) {
							specificError = new USGError(hour, minute, second, milliseconds, who, error, message);
						} else if ((unnamed.equals("PAL")) && (origin.equals("STT"))) {
							specificError = new PaletteError(hour, minute, second, milliseconds, who, error, message);
						}
					} else if (origin.equals("STA")) {
						specificError = new StatusError(hour, minute, second, milliseconds, who, error, message);
					} else if (!error.equals("00")) {
						bw.write(line + "\n");
					}
					if ((specificError != null) && (specificError.isError())) {
						bw.write(specificError + "\n");
					}
/*					if (pending3.equals("FEH")) {
						if (pending.equals("STR")) {
							STRError errorSTR = new STRError(message);
							if (!errorSTR.getError().equals(ERROR_VALUE)) {
								bw.write(line + "\n");
							}
						}
						//bw.write(hour + ":" + minute + ":" + second + ":" + milliseconds + "(" + counter + ")" + "[" + origin + "]" + "[" + element + "]" + "[" + pending + "]" + "Error:" + error + "\t" + message + "\n");
					} else  if (!error.equals("00")) {
					}*/
/*					String hour = matcher.group(14);
					String to = matcher.group(15);
					String type = matcher.group(16);
					if ((type.compareToIgnoreCase("A") == 0)
							&& (to.compareToIgnoreCase("ELR") == 0)
							&& (from.compareToIgnoreCase("WAWI") != 0)) {
						String message = matcher.group(17);
						String[] messageTokens = message.split("\\|");
						String codigo = messageTokens[6];
						if ((codigo.endsWith("852582"))
								|| (codigo.endsWith("734681"))) {
							String year = matcher.group(1);
							String month = matcher.group(2);
							String day = matcher.group(3);
							String hour = matcher.group(4);
							String minute = matcher.group(5);
							String second = matcher.group(6);
							String miliseconds = matcher.group(7);
							String orderNumber = messageTokens[1];
							String sellPoint = messageTokens[2];
							String robotDoor = messageTokens[3];
							String state = messageTokens[4];
							// String numberOfLines = messageTokens[5];
							String quantity = messageTokens[7];
							// bw.write("Line[" + i + "]:" + year + "/" + month
							// + "/" + day + " " + hour + ":" + minute + ":" +
							// second + "." + miliseconds + " " + from + " -> "
							// + to + " " + type +" " + message + "\n");
							bw.write("Line[" + i + "]:" + year + "/" + month
									+ "/" + day + " " + hour + ":" + minute
									+ ":" + second + "." + miliseconds
									+ " [ENTREGA]: ");
							bw.write("Producto = " + codigo
									+ ". N� de pedido = " + orderNumber
									+ " del TPV " + sellPoint
									+ ". Dispensado por " + robotDoor
									+ ". Estado " + aMessageState(state)
									+ " Cantidad = " + quantity + "\n");
							// System.out.println("Line[" + i + "]:" + year +
							// "/" + month + "/" + day + " " + hour + ":" +
							// minute + ":" + second + "." + miliseconds + " " +
							// from + " -> " + to + " " + type +" " + message);
						}
					} else if ((type.compareToIgnoreCase("E") == 0)
							&& (to.compareToIgnoreCase("ELR") == 0)) {
						String message = matcher.group(17);
						String[] messageTokens = message.split("\\|");
						String codigo = messageTokens[2];
						if ((codigo.endsWith("852582"))
								|| (codigo.endsWith("734681"))) {
							String year = matcher.group(1);
							String month = matcher.group(2);
							String day = matcher.group(3);
							String hour = matcher.group(4);
							String minute = matcher.group(5);
							String second = matcher.group(6);
							String miliseconds = matcher.group(7);
							String quantity = messageTokens[4];
							String expirationDate = messageTokens[5];
							// bw.write("Line[" + i + "]:" + year + "/" + month
							// + "/" + day + " " + hour + ":" + minute + ":" +
							// second + "." + miliseconds + " " + from + " -> "
							// + to + " " + type +" " + message + "\n");
							bw.write("Line[" + i + "]:" + year + "/" + month
									+ "/" + day + " " + hour + ":" + minute
									+ ":" + second + "." + miliseconds
									+ " [ENTRADA]: ");
							bw.write("Producto = " + codigo + ". Cantidad = "
									+ quantity + " Expiration Date = "
									+ expirationDate + "\n");
							// System.out.println("Line[" + i + "]:" + year +
							// "/" + month + "/" + day + " " + hour + ":" +
							// minute + ":" + second + "." + miliseconds + " " +
							// from + " -> " + to + " " + type +" " + message);
						}
					}
				} else {
					// System.out.println("Line " + i + " has not been parsed");*/
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (null != fr) {
				try {
					fr.close();
				} catch (IOException e2) {
					System.out.println(e2.getMessage());
				}
			}
			if (null != bw) {
				try {
					bw.close();
				} catch (IOException e2) {
					System.out.println(e2.getMessage());
				}
			}
		}
		return false;
	}

}
