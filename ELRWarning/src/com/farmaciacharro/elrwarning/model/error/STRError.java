package com.farmaciacharro.elrwarning.model.error;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.farmaciacharro.elrwarning.model.almacenaje.Robot;

public class STRError extends Error{

	private final static String NAME = "STR";
	
	private Integer x;
	private Integer y;
	private Integer lado;
	private Integer z;
	private Integer k;
	private String error;
	
	public STRError(String hour, String minute, String second, 
			String milliseconds, String who, String error, String message) {
		super(hour, minute, second, milliseconds, who, error, NAME);
		parseLine(message);
	}

	@Override
	public void parseLine(String line) {
		Pattern pattern = Pattern
				.compile("(\\w+);\\s+(\\d+);\\s+(\\d+);\\s+(\\d+);\\s+(\\d+);\\s+(\\d+);\\s+(\\d+);\\s+(\\d+);\\s+(\\w+);\\s+(\\d+);\\s+(\\d+);\\s+(\\d+);\\s+(.+)");
		Matcher matcher = pattern.matcher(line);
		if (matcher.find()) {
			String info = matcher.group(1);
			//String unknown = matcher.group(2);
			//String unknown2 = matcher.group(3);
			x = Integer.valueOf(matcher.group(4));
			y = Integer.valueOf(matcher.group(5));
			lado = Integer.valueOf(matcher.group(6));
			z = Integer.valueOf(matcher.group(7));
			k = Integer.valueOf(matcher.group(8));
			//String unknown3 = matcher.group(9);
			error = matcher.group(10);
			//String unknown4 = matcher.group(11);
			//String unknown5 = matcher.group(12);
			//String unknown6 = matcher.group(13);
			if ((info.equals("FEH")) && (!error.equals(Error.ERROR_VALUE))) {
				this.realError = true;
			}
		}
	}
	
	public Integer getX() {
		return this.x;
	}
	
	public Integer getY() {
		return this.y;
	}
	
	public Integer getLado() {
		return this.lado;
	}
	
	public Integer getZ() {
		return this.z;
	}
	
	public Integer getK() {
		return this.k;
	}
	
	public String getError() {
		return this.error;
	}

	private String errorMeaning() {
		String meaning = "";
		// Primer bit
		int bit = hex2Int(error.substring(0, 1));
		if (bit >= 7) {
			meaning += "Regulador Y parado\t";
			bit -= 8;
		}
		if (bit > 3) {
			meaning += "Regulador X parado\t";
			bit -= 4;
		}
		if (bit > 1) {
			meaning += "Fin de carrera Y\t";
			bit -= 2;
		}
		if (bit > 0) {
			meaning += "Fin de carrera X\t";
			bit -= 1;
		}
		// Segundo bit
		bit = hex2Int(error.substring(1, 2));
		if (bit >= 7) {
			meaning += "Bloqueo en eje K del LAM\t";
			bit -= 8;
		}
		if (bit > 3) {
			meaning += "Bloqueo en eje Z del LAM\t";
			bit -= 4;
		}
		if (bit > 1) {
			meaning += "HHG no está en zona e Pos Defect\t";
			bit -= 2;
		}
		if (bit > 0) {
			meaning += "Error de spaltz o de ini Z\t";
			bit -= 1;
		}
		// Tercer bit
		bit = hex2Int(error.substring(2, 3));
		if (bit >= 7) {
			meaning += "Fallo comunicación CAN-BUS del Macs\t";
			bit -= 8;
		}
		if (bit > 3) {
			meaning += "Macs del LAM con sesión no iniciada\t";
			bit -= 4;
		}
		if (bit > 1) {
			meaning += "Sin referencia de K en el LAM\t";
			bit -= 2;
		}
		if (bit > 0) {
			meaning += "Sin referencia de Z en el LAM\t";
			bit -= 1;
		}
		// Cuarto bit
		bit = hex2Int(error.substring(3, 4));
		if (bit >= 7) {
			meaning += "APOClean Error\t";
			bit -= 8;
		}
		if (bit > 3) {
			meaning += "Pinza no está en Groundposs o sensorZ averiado\t";
			bit -= 4;
		}
		if (bit > 1) {
			meaning += "Timeout de giro\t";
			bit -= 2;
		}
		if (bit > 0) {
			meaning += "Timeout de secuencia\t";
			bit -= 1;
		}
		// Quinto bit
		bit = hex2Int(error.substring(4, 5));
		if (bit >= 7) {
			meaning += "Sin especificar\t";
			bit -= 8;
		}
		if (bit > 3) {
			meaning += "Sin especificar\t";
			bit -= 4;
		}
		if (bit > 1) {
			meaning += "Bloqueo en eje de giro del LAM\t";
			bit -= 2;
		}
		if (bit > 0) {
			meaning += "Sin referencia en eje de giro LAM\t";
			bit -= 1;
		}
		return meaning;	
	}
	
	public String toString() {
		return super.toString() + "[" + lado + ":(" + x + "," + y + "," + z + "," + k + ")] = " + Robot.getPosicion(lado, x, y) + " " + error + "(" + errorMeaning() + ")";
	}
}
