package com.farmaciacharro.elrwarning.model.error;

public abstract class Error {

	protected static final String ERROR_VALUE = "00000000000000000000";

	private String hour;
	private String minute;
	private String second;
	private String milliseconds;
	private String who;
	private String error;
	private String name;
	protected boolean realError;
	
	public Error(String hour, String minute, String second, 
			String milliseconds, String who, String error, String name) {
		this.hour = hour;
		this.minute = minute;
		this.second = second;
		this.milliseconds = milliseconds;
		this.who = who;
		this.error = error;
		this.name = name;
		this.realError = false;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getHour() {
		return hour;
	}

	public void setHour(String hour) {
		this.hour = hour;
	}

	public String getMinute() {
		return minute;
	}

	public void setMinute(String minute) {
		this.minute = minute;
	}

	public String getSecond() {
		return second;
	}

	public void setSecond(String second) {
		this.second = second;
	}

	public String getMilliseconds() {
		return milliseconds;
	}

	public void setMilliseconds(String milliseconds) {
		this.milliseconds = milliseconds;
	}

	public String getWho() {
		return who;
	}

	public void setWho(String who) {
		this.who = who;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	public boolean isError() {
		return this.realError;
	}

	public abstract void parseLine(String line);
	
	protected int hex2Int(String hex) {
		if (hex.equalsIgnoreCase("a")) {
			return 10;
		} else if (hex.equalsIgnoreCase("b")) {
			return 11;
		} else if (hex.equalsIgnoreCase("c")) {
			return 12;
		} else if (hex.equalsIgnoreCase("d")) {
			return 13;
		} else if (hex.equalsIgnoreCase("e")) {
			return 14;
		} else if (hex.equalsIgnoreCase("f")) {
			return 15;
		}
		return Integer.valueOf(hex);
	}
	
	public String toString() {
		return hour + ":" + minute + ":" + second + "." + milliseconds + "[" + name + ":" + who + "]";
	}
}
