package com.farmaciacharro.elrwarning.model.error;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class USGError extends Error {

	private final static String NAME = "USG";
	
	private String error;

	public USGError(String hour, String minute, String second, 
			String milliseconds, String who, String error, String message) {
		super(hour, minute, second, milliseconds, who, error, NAME);
		parseLine(message);
	}

	@Override
	public void parseLine(String line) {
		Pattern pattern = Pattern
				//.compile("(\\w+);\\s+(\\d+);\\s+(\\d+);\\s+(\\d+);\\s+(\\d+);\\s+(\\d+);\\s+(\\d+);\\s+(\\d+);\\s+(\\w+);\\s+(\\d+);\\s+(\\d+);\\s+(\\d+);\\s+(.+)");
				.compile("(\\w+);\\s+(\\d+);\\s+(\\d+);\\s+(\\d+);\\s+(\\w+);\\s+(\\d+);\\s+(\\d+);\\s+(.+)");
		Matcher matcher = pattern.matcher(line);
		if (matcher.find()) {
			String info = matcher.group(1);
			//String unknown = matcher.group(2);
			//String unknown2 = matcher.group(3);
			error = matcher.group(5);
			//String unknown4 = matcher.group(11);
			//String unknown5 = matcher.group(12);
			//String unknown6 = matcher.group(13);
			if ((info.equals("FEH")) && (!error.equals(Error.ERROR_VALUE))) {
				this.realError = true;
			}
		}
	}
	
	private String errorMeaning() {
		String meaning = "";
		// Primer bit
		int bit = hex2Int(error.substring(0, 1));
		if (bit >= 7) {
			meaning += "Fallo de comunicaciones\t";
			bit -= 8;
		}
		if (bit > 3) {
			meaning += "MACS parado\t";
			bit -= 4;
		}
		if (bit > 1) {
			meaning += "Eje Z o cinta ELB bloqueado\t";
			bit -= 2;
		}
		if (bit > 0) {
			meaning += "Referencia Z de USB o ELB no tomada\t";
			bit -= 1;
		}
		// Segundo bit
		bit = hex2Int(error.substring(1, 2));
		if (bit >= 7) {
			meaning += "Sin especificar\t";
			bit -= 8;
		}
		if (bit > 3) {
			meaning += "Timeout\t";
			bit -= 4;
		}
		if (bit > 1) {
			meaning += "Error de eje K\t";
			bit -= 2;
		}
		if (bit > 0) {
			meaning += "Referencia de K no tomada\t";
			bit -= 1;
		}
		return meaning;
	}

	public String toString() {
		return super.toString() + " = " + error + "(" + errorMeaning() + ")";
	}
}
