package com.farmaciacharro.elrwarning.model.error;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StatusError extends Error {

	private final static String NAME = "STA";

	private String error;

	public StatusError(String hour, String minute, String second, 
			String milliseconds, String who, String error, String message) {
		super(hour, minute, second, milliseconds, who, error, NAME);
		parseLine(message);
	}

	@Override
	public void parseLine(String line) {
		String[] aux = line.split("; ");
//		Pattern pattern = Pattern
//				.compile("(\\w+);\\s+(\\w+);\\s+(\\d+)\\s+(.+)");
//		Matcher matcher = pattern.matcher(line);
		String info = aux[0];
		if (info.equals("IF")) {
			error = aux[1];
			if (info.equals("IF")) {
				this.realError = true;
			}
		}
	}
	
	private String errorMeaning() {
		String meaning = "";
		// Primer bit
		int bit = hex2Int(error.substring(0, 1));
		if (bit >= 7) {
			meaning += "Puerta 2 abierta\t";
			bit -= 8;
		}
		if (bit > 3) {
			meaning += "Puerta 1 abierta\t";
			bit -= 4;
		}
		if (bit > 1) {
			meaning += "Sin potencia\t";
			bit -= 2;
		}
		if (bit > 0) {
			meaning += "Parada de emergencia\t";
			bit -= 1;
		}
		// Segundo bit
		bit = hex2Int(error.substring(1, 2));
		if (bit >= 7) {
			meaning += "Límite de carrera Y alcanzado\tBloqueado, giro no posible o brazos muy juntos\t";
			bit -= 8;
		}
		if (bit > 3) {
			meaning += "Error general\t";
			bit -= 4;
		}
		if (bit > 1) {
			meaning += "Falsche telegramm-Nr empfangen";
			bit -= 2;
		}
		if (bit > 0) {
			meaning += "Fuente DC indica sobretensión\t";
			bit -= 1;
		}
		// Tercer bit
		bit = hex2Int(error.substring(2, 3));
		if (bit >= 7) {
			meaning += "Uno de los reguladores no activo\t";
			bit -= 8;
		}
		if (bit > 3) {
			meaning += "No está en posición por defecto (fallo)\t";
			bit -= 4;
		}
		if (bit > 1) {
			meaning += "Nevera no preparada\t";
			bit -= 2;
		}
		if (bit > 0) {
			meaning += "Fallo al cargar la Par_file.dat\t";
			bit -= 1;
		}		return meaning;
	}
	
	public String toString() {
		return super.toString() + " = " + error + "(" + errorMeaning() + ")";
	}
}
