package com.farmaciacharro.elrwarning.model.error;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PaletteError extends Error {

	private final static String NAME = "PAL";
	
	private String error;

	public PaletteError(String hour, String minute, String second, 
			String milliseconds, String who, String error, String message) {
		super(hour, minute, second, milliseconds, who, error, NAME);
		parseLine(message);
	}

	@Override
	public void parseLine(String line) {
		Pattern pattern = Pattern
				.compile("(\\w+);\\s+(\\d+);\\s+(\\d+);\\s+(\\d+);\\s+(\\d+);\\s+(\\w+);\\s+(.+)");
		Matcher matcher = pattern.matcher(line);
		if (matcher.find()) {
			String info = matcher.group(1);
			error = matcher.group(6);
			if ((info.equals("FEH")) && (!error.equals(Error.ERROR_VALUE))) {
				this.realError = true;
			}
		}
	}
	
	private String errorMeaning() {
		String meaning = "";
		// Primer bit
		int bit = hex2Int(error.substring(0, 1));
		if (bit >= 7) {
			meaning += "Fallo de comunicaciones\t";
			bit -= 8;
		}
		if (bit > 3) {
			meaning += "Status\t";
			bit -= 4;
		}
		if (bit > 1) {
			meaning += "Fallo en X\t";
			bit -= 2;
		}
		if (bit > 0) {
			meaning += "Sin referencias en X\t";
			bit -= 1;
		}
		// Segundo bit
		bit = hex2Int(error.substring(1, 2));
		if (bit >= 7) {
			meaning += "Producto entre el brazo y el cristal\t";
			bit -= 8;
		}
		if (bit > 3) {
			meaning += "Timeout\t";
			bit -= 4;
		}
		if (bit > 1) {
			meaning += "Fallo en Y\t";
			bit -= 2;
		}
		if (bit > 0) {
			meaning += "Sin referencias en Y\t";
			bit -= 1;
		}
		// Tercer bit
		bit = hex2Int(error.substring(2, 3));
		if (bit >= 7) {
			meaning += "Fallo desacoplando\t";
			bit -= 8;
		}
		if (bit > 3) {
			meaning += "Fallo desacoplando\t";
			bit -= 4;
		}
		if (bit > 1) {
			meaning += "Time out, cerrar seguro, solenoides dentro\t";
			bit -= 2;
		}
		if (bit > 0) {
			meaning += "Time out, abrir seguro, solenoides fuera\t";
			bit -= 1;
		}
		return meaning;	
	}

	public String toString() {
		return super.toString() + " = " + error + "(" + errorMeaning() + ")";
	}
}
