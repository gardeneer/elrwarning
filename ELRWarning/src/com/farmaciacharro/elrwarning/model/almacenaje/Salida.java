package com.farmaciacharro.elrwarning.model.almacenaje;

public class Salida extends Posicion {

	public final static String PREFIJO = "SAL";
	
	private String nombre;
	private int numeroWawi;
	
	public Salida(String linea) {
		super(1, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE);
		parseLinea(linea);
	}
	
	private void parseLinea(String linea) {
		String[] pedazos = linea.split("\t");
		try {
			numeroWawi = Integer.valueOf(pedazos[5]);
			nombre = pedazos[18];
			xIni = Math.min(xIni, Integer.valueOf(pedazos[7]) - holgura);
			xFin = Math.max(xFin, Integer.valueOf(pedazos[7]) + holgura);
			yIni = Math.min(yIni, Integer.valueOf(pedazos[8]) - holgura);
			yFin = Math.max(yFin, Integer.valueOf(pedazos[8]) + holgura);
		} catch (NumberFormatException nfe) {
			LOGGER.severe("Error al formatear numeros en las salidas:" + nfe.getMessage());
		}		
	}
	
	public void setEspacioFisico(int xIni, int xFin, int yIni, int yFin) {
		this.xIni = Math.min(this.xIni, xIni - holgura);
		this.xFin = Math.max(this.xFin, xFin + holgura);
		this.yIni = Math.min(this.yIni, yIni - holgura);
		this.yFin = Math.max(this.yFin, yFin + holgura);
	}
	
	public String getNombre() {
		return PREFIJO + nombre;
	}

	public int getNumeroWawi() { 
		return numeroWawi;
	}
	
	public String toString() {
		return "[" + numeroWawi + "]" + getNombre() + "[" + xIni + ", " + xFin + ", " + yIni + ", " + yFin + "]";		
	}
}
