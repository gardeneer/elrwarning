package com.farmaciacharro.elrwarning.model.almacenaje;

import java.util.logging.Logger;

import net.icolino.util.log.Log;

public abstract class Posicion {

	protected final static Logger LOGGER = Logger
			.getLogger(Log.LOGNAME);

	protected static int holgura = 50; // Decimas de mm
	protected int lado;
	protected int xIni;
	protected int xFin;
	protected int yIni;
	protected int yFin;
	
	public Posicion (int lado, int xIni, int xFin, int yIni, int yFin) {
		this.lado = lado;
		this.xIni = xIni;
		this.xFin = xFin;
		this.yIni = yIni;
		this.yFin = yFin;
	}
	
	public boolean estaDentro(int x, int y) {
		boolean dentro = false;
		if ((xIni <= x) && (xFin >= x)) {
			if ((yIni <= y) && (yFin >= y)) {
				dentro = true;
			}
		}
		return dentro;
	}

	public abstract String getNombre();

	public int getLado() {
		return lado;
	}

	public int getXIni() {
		return xIni;
	}

	public int getXFin() {
		return xFin;
	}

	public int getYIni() {
		return yIni;
	}

	public int getYFin() {
		return yFin;
	}
}
