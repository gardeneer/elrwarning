package com.farmaciacharro.elrwarning.model.almacenaje;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.icolino.util.log.Log;

public class Robot {

	protected final static Logger LOGGER = Logger
			.getLogger(Log.LOGNAME);
	
	private final static String ARCHIVO_COLUMNAS = "Steher.txt";
	private final static String ARCHIVO_CRISTALES = "Fachboden.txt";
	private final static String ARCHIVO_CRISTALES_FISICOS = "PhyFachboden.txt";
	private final static String ARCHIVO_BANDEJAS = "Palette.txt";
	private final static String ARCHIVO_SALIDAS = "Bahnof.txt";
	
	private static Map<String, Posicion> posiciones = new HashMap<String, Posicion>();
	
	private Robot() {}
	
	public static void parseFiles(String directory) {
		Map<String, Columna> columnas = new HashMap<String, Columna>();
		try {
			File file = new File(directory + ARCHIVO_COLUMNAS);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String linea;
			while ((linea = br.readLine()) != null) {
				Pattern pattern = Pattern
						.compile("(\\d+)(.+)");
				Matcher matcher = pattern.matcher(linea);
				if (matcher.find()) {
					Columna c = new Columna(linea);
					columnas.put(c.getEstanteria() + "-" + c.getColumna(), c);
				} else {
					LOGGER.info("Imposible parsear columna : " + linea);
				}
			}
			br.close();
			fr.close();
			file = new File(directory + ARCHIVO_CRISTALES);
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			while ((linea = br.readLine()) != null) {
				Pattern pattern = Pattern
						.compile("(\\d+)\\t(\\d+)\\t(.+)");
				Matcher matcher = pattern.matcher(linea);
				if (matcher.find()) {
					Cristal c = new Cristal(linea);
					c.setColumna(columnas.get(c.getLado() + "-" + c.getColumnaId()));
					posiciones.put(Cristal.PREFIJO + c.getIndice(), c);
				} else {
					LOGGER.info("Imposible parsear cristal : " + linea);
				} 
			}
			br.close();
			fr.close();
			file = new File(directory + ARCHIVO_CRISTALES_FISICOS);
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			while ((linea = br.readLine()) != null) {
				Pattern pattern = Pattern
						.compile("(\\d+)\\t(\\d+)\\t(\\d+)\\|(\\d+)\\t(\\d+)\\t(\\d+)\\t(\\d+)\\t(\\d+)\\t(\\d+)\\t(.+)");
				Matcher matcher = pattern.matcher(linea);
				if (matcher.find()) {
					Integer index = Integer.valueOf(matcher.group(5));
					Integer xIni = Integer.valueOf(matcher.group(7));
					Integer yIni = Integer.valueOf(matcher.group(8));
					Integer yFin = Integer.valueOf(matcher.group(9));
					
					((Cristal)posiciones.get(Cristal.PREFIJO + index)).setEspacioFisico(xIni, yIni, yFin);
				} else {
					LOGGER.info("Imposible parsear cristal fisico : " + linea);
				} 
			}
			br.close();
			fr.close();
			file = new File(directory + ARCHIVO_BANDEJAS);
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			while ((linea = br.readLine()) != null) {
				Pattern pattern = Pattern
						.compile("(\\d+)\\t(\\d+)\\t(.+)");
				Matcher matcher = pattern.matcher(linea);
				if (matcher.find()) {
					Bandeja b = new Bandeja(linea);
					posiciones.put(Bandeja.PREFIJO + b.getIndice(), b);
				} else {
					LOGGER.info("Imposible parsear bandeja : " + linea);
				} 
			}
			br.close();
			fr.close();
			br.close();
			fr.close();
			file = new File(directory + ARCHIVO_SALIDAS);
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			
			while ((linea = br.readLine()) != null) {
				Pattern pattern = Pattern
						.compile("(\\d+)\\t(\\d+)\\t(.+)");
				Matcher matcher = pattern.matcher(linea);
				if (matcher.find()) {
					Salida s = new Salida(linea);
					if (posiciones.get(Salida.PREFIJO + s.getNumeroWawi()) != null) {
						((Salida)posiciones.get(Salida.PREFIJO + s.getNumeroWawi())).setEspacioFisico(s.getXIni(), s.getXFin(), s.getYIni(), s.getYFin());
					} else {
						posiciones.put(Salida.PREFIJO + s.getNumeroWawi(), s);
					}
				} else {
					LOGGER.info("Imposible parsear salida : " + linea);
				} 
			}
			br.close();
			fr.close();
			Pasillo p = new Pasillo();
			posiciones.put(Pasillo.PREFIJO, p);
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
			LOGGER.severe("Error en Robot " + ioe.getMessage());
		}
	}
	
	public static String getPosicion(int lado, int x, int y) {
		String posicion = "";
		for (String i: Robot.posiciones.keySet()) {
			Posicion aux = posiciones.get(i);
			if (aux.getLado() == lado) {
				if (aux.estaDentro(x, y)) {
					posicion = aux.getNombre();
					break;
				}
			} 
		}
		if (posicion == "") { 
			LOGGER.info("Posicion no encontrada : " + lado + "[" + x + ", " + y + "]");
			posicion = "Indeterminada";
		}
		return posicion;
	}
	
	public static void imprimirPosiciones() {
		for (String i: Robot.posiciones.keySet()) {
			LOGGER.severe(Robot.posiciones.get(i).toString());
		}
	}
}
