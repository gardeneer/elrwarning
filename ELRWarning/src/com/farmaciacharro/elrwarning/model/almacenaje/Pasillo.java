package com.farmaciacharro.elrwarning.model.almacenaje;

public class Pasillo extends Posicion {

	public final static String PREFIJO = "PAS";
	
	public Pasillo() {
		super(0, 0, Integer.MAX_VALUE, 0, Integer.MAX_VALUE);
	}
	
	public String getNombre() {
		return PREFIJO;
	}
	
	public String toString() {
		return getNombre();
	}

}
