package com.farmaciacharro.elrwarning.model.almacenaje;

public class Bandeja extends Posicion {

	public final static String PREFIJO = "PAL";
	
	private int ancho;
	private int profundidad;
	private int indice;
	private String nombre;

	public Bandeja(String linea) {
		super(1, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE);
		parseLinea(linea);
	}
	
	private void parseLinea(String linea) {
		String[] pedazos = linea.split("\t");
		try {
			indice = Integer.valueOf(pedazos[0]);
			nombre = pedazos[3];
			ancho = Integer.valueOf(pedazos[5]);
			profundidad = Integer.valueOf(pedazos[6]);
			xIni = Math.min(Integer.valueOf(pedazos[16]), Integer.valueOf(pedazos[23]));
			xFin = Math.max(Integer.valueOf(pedazos[19]), Integer.valueOf(pedazos[26]));
			yIni = Math.min(Integer.valueOf(pedazos[17]), Integer.valueOf(pedazos[24])) - holgura;
			yFin = Math.max(Integer.valueOf(pedazos[20]), Integer.valueOf(pedazos[27])) + holgura;
		} catch (NumberFormatException nfe) {
			LOGGER.severe("Error al formatear numeros en las bandejas:" + nfe.getMessage());
		}
	}

	public int getAncho() {
		return ancho;
	}

	public int getProfundidad() {
		return profundidad;
	}
	
	public String getNombre() {
		return PREFIJO + nombre;
	}
	
	public int getIndice() {
		return indice;
	}
	
	public String toString() {
		return "[" + indice + "]" + getNombre() + "[" + xIni + ", " + xFin + ", " + yIni + ", " + yFin + "]";
	}

}
