package com.farmaciacharro.elrwarning.model.almacenaje;

public class Cristal extends Posicion {

	private int indice;
	private int columnaId;
	private int alto;
	private int ancho;
	private int fondo;
	private Columna columna;
	public final static String PREFIJO = "CRI";
	private String nombre = "";
	
	public Cristal(String linea) {
		super(1, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE);
		parseLinea(linea);
	}
	
	public void setEspacioFisico(int xIni, int yIni, int yFin) {
		this.xIni = Math.min(this.xIni, xIni);
		this.xFin = Math.max(this.xFin, xIni + ancho + holgura);
		this.yIni = Math.min(this.yIni, Math.min(yIni, yFin) - holgura);
		this.yFin = Math.max(this.yFin, Math.max(yIni, yFin) + holgura);
	}
	
	public int getIndice() {
		return indice;
	}

	public void setIndice(int indice) {
		this.indice = indice;
	}

	public int getAlto() {
		return alto;
	}

	public void setAlto(int alto) {
		this.alto = alto;
	}

	public int getAncho() {
		return ancho;
	}

	public void setAncho(int ancho) {
		this.ancho = ancho;
	}

	public int getFondo() {
		return fondo;
	}

	public void setFondo(int fondo) {
		this.fondo = fondo;
	}

	public int getColumnaId() {
		return columnaId;
	}

	public void setColumnaId(int columnaId) {
		this.columnaId = columnaId;
	}

	public Columna getColumna() {
		return columna;
	}

	public void setColumna(Columna columna) {
		this.columna = columna;
	}
	
	public String getNombre() {
		return PREFIJO + columna + nombre;
	}

	private void parseLinea(String linea) {
		String[] pedazos = linea.split("\t");
		try {
			indice = Integer.valueOf(pedazos[0]);
			lado = Integer.valueOf(pedazos[3]);
			columnaId = Integer.valueOf(pedazos[4]);
			nombre = pedazos[5];
			alto = Integer.valueOf(pedazos[6]);
			ancho = Integer.valueOf(pedazos[9]);
			fondo = Integer.valueOf(pedazos[11]);
		} catch (NumberFormatException nfe) {
			LOGGER.severe("Error al formatear numeros en los cristales:" + nfe.getMessage());
		}
	}
	
	public String toString() {
		return "[" + indice + "]" + getNombre() + "[" + xIni + ", " + xFin + ", " + yIni + ", " + yFin + "]";
	}
}
