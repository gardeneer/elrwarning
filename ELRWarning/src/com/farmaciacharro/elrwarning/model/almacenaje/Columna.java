package com.farmaciacharro.elrwarning.model.almacenaje;

public class Columna {

	private int indice;
	private int estanteria;
	private int columna;
	private String nombre;
	
	public Columna(String linea) {
		parseLinea(linea);
	}
	
	public int getIndice() {
		return indice;
	}

	public void setIndice(int indice) {
		this.indice = indice;
	}

	public int getEstanteria() {
		return estanteria;
	}

	public void setEstanteria(int estanteria) {
		this.estanteria = estanteria;
	}

	public int getColumna() {
		return columna;
	}

	public void setColumna(int columna) {
		this.columna = columna;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	private void parseLinea(String linea) {
		String[] pedazos = linea.split("\t");
		try {
			indice = Integer.valueOf(pedazos[0]);
			estanteria = Integer.valueOf(pedazos[3]);
			columna = Integer.valueOf(pedazos[4]);
			nombre = pedazos[5];
		} catch (NumberFormatException nfe) {}
	}
	
	public String toString() {
		return estanteria + nombre;
	}
}
