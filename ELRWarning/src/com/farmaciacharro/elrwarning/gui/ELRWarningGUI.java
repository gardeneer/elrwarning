package com.farmaciacharro.elrwarning.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ELRWarningGUI extends JFrame {

	private static final long serialVersionUID = 1L;

	private static ELRWarningGUI instance = null;
	
	private ELRWarningGUI() {
		init();
	}
	
	public static ELRWarningGUI getInstance() {
		if (null == instance) {
			instance = new ELRWarningGUI();
		}
		return instance;
	}
	
	private void init() {
		setTitle("Apostore Log Analyzer Tool");
		setSize(400, 400);
		
		JLabel databaseLabel = new JLabel("Database");
		JTextField databaseField = new JTextField("");
		databaseField.setSize(80, 20);
		JButton databaseButton = new JButton("Search Database");
		
		JLabel filesLabel = new JLabel("Files");
		JTextField filesField = new JTextField("");
		filesField.setSize(80, 20);
		JButton filesButton = new JButton("Search Files");
		
		JLabel savefileLabel = new JLabel("Save to");
		JTextField savefileField = new JTextField("");
		savefileField.setSize(80, 20);
		JButton savefileButton = new JButton("Save to");
		
		JPanel pathPanel = new JPanel();
		pathPanel.setLayout(new GridLayout(3, 3));
		pathPanel.add(databaseLabel);
		pathPanel.add(databaseField);
		pathPanel.add(databaseButton);
		pathPanel.add(filesLabel);
		pathPanel.add(filesField);
		pathPanel.add(filesButton);
		pathPanel.add(savefileLabel);
		pathPanel.add(savefileField);
		pathPanel.add(savefileButton);
		
		JButton analyzeButton = new JButton("Analyze");
		
//		JFileChooser databaseFiles = new JFileChooser();
//		databaseFiles.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(pathPanel, BorderLayout.CENTER);
		getContentPane().add(analyzeButton, BorderLayout.SOUTH);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	public static void main (String args[]) {
		new ELRWarningGUI();
	}
}
